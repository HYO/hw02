// simple_int_set.h

#ifndef _SIMPLE_INT_SET_H_
#define _SIMPLE_INT_SET_H_

class SimpleIntSet {
 public:
  IntSet();

  IntSet Intersect(const IntSet& int_set) const;
  IntSet Union(const IntSet& int_set) const;
  IntSet Difference(const IntSet& int_set) const;

  void Set(const int* values, int size);

  const int* values() const { return values_; }
  int size() const { return size_; }

 private:
  const int MAX_SIZE 100;
  int values_[MAX_SIZE];
  int size_;
};

#endif  // _SIMPLE_INT_SET_H_
