# Makefile

all: radix_notation draw_points simple_calculator simple_int_set
	g++ -o radix_notation radix_notation.cc
	g++ -o draw_points draw_points.cc
	g++ -o simple_calculator siple_calculator.cc simple_calculator_main.cc
	g++ -o simple_int_set simple_int_set.cc simple_int_set_main.cc