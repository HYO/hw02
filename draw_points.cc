// draw_points.cc

#include <iostream>
using namespace std;

int num,max_x,max_y;
struct Point {
  unsigned int x, y;
  
};

void DrawPoints(const Point* points, int num_points) {
  int k=0;
  for(int i=0; i<num; i++) {
	if((points[i].x+points[i].y*max_x)==num_points) {
		cout<<"* ";
		k=1;
		break;
 	}
  }
  if (k==0) cout<<". ";
}
int main() {
  cin>>num;
  Point* arr=new Point[num];
  for(int i=0;i<num;i++) {
	cin>>arr[i].x >>arr[i].y;
  }
  max_x=arr[0].x;
  max_y=arr[0].y;
  for(int i=1; i<num;i++) {
	if(max_x<arr[i].x) max_x=arr[i].x;
	if(max_y<arr[i].y) max_y=arr[i].y;
  }
  max_x=max_x+1;
  for(int j=0;j<=max_y;j++) {
	for(int i=0;i<max_x;i++) 
		DrawPoints(arr,j*max_x+i);
	cout<<endl;
  }
  return 0;
}