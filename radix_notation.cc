// radix_notation.cc

#include <string>
#include <iostream>

using namespace std;

// Implement this function.
string RadixNotation(unsigned int number, unsigned int radix);

int main() {
  while (true) {
    unsigned int number, radix;
    cin >> number >> radix;
    if (radix < 2 || radix > 36) break;
    cout << RadixNotation(number, radix) << endl;
  }
  return 0;
}

string RadixNotation(unsigned int number, unsigned int radix) {
    string x;
    char tmp;
    int a=number%radix;
    number=number/radix;
    if (number>=radix) x=RadixNotation(number,radix);
    else {
    if (number<10) cout<<number;
    else {
         tmp='a'+(number-10); 
         cout<<tmp;
    }
    }
    if (a<10) cout<<a;
    else {tmp='a'+(a-10); cout<<tmp;}
    return " ";
}