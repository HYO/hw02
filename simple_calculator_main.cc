// simple_calculator_main.cc
#include<iostream>
#include "simple_calculator.h"
using namespace std;

int main() {
  int num;
  double num2;
  cin>>num;
  SimpleCalculator calculator(num);
  char cal;
  cin>>cal;
  while (cal!='=') {
	cin>>num2;
	if (cal=='+')
	  calculator.Add(num2);
	else if (cal=='-')
	  calculator.Subtract(num2);
	else if (cal=='*')
	  calculator.Multiply(num2);
	else if (cal=='/')
	  calculator.Divide(num2);
	else return 0;
        cin>>cal;
  }
  cout<< calculator.value()<<endl;
  return 0;
}